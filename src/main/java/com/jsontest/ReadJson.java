package com.jsontest;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class ReadJson {

    
    @GetMapping("/readJson/{powerSupplyName}")
    public Object readJson(@PathVariable("powerSupplyName") String powerSupplyName){
    	ObjectMapper mapper = new ObjectMapper();
    	MyJson myJson = null;
		TypeReference<MyJson> typeReference = new TypeReference<MyJson>(){};
		InputStream inputStream = TypeReference.class.getResourceAsStream("/public/"+powerSupplyName);
		try {
			myJson = mapper.readValue(inputStream,typeReference);
			System.out.println(myJson);
			StringBuffer strB = new StringBuffer();
			List<FeaturesJson> features =  myJson.getFeaturesJson();
			for(FeaturesJson featuresJson : features){
				PropertiesJson  json = featuresJson.getPropertiesJson();
				if(json.getCox_node_number() != null && !json.getCox_node_number().isEmpty() && !"null".equalsIgnoreCase(json.getCox_node_number())){
					strB.append("Update powerlatlong set node = '" + json.getCox_node_number() + "' Where device_name = '" + json.getName()
							+ "' and cox_site_code = '" + json.getCox_site_code() + "';\n");
				}
			}
			
		    BufferedWriter writer = new BufferedWriter(new FileWriter(new File(powerSupplyName+".txt")));
		    writer.write(strB.toString());
		     
		    writer.close();
			
		} catch (Exception e){
			System.out.println("Unable Read: " + e.getMessage());
		}
		return myJson;
    }

}
