package com.jsontest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeaturesJson {

	private String type;
	@JsonProperty("properties")
	private PropertiesJson propertiesJson;

	public String getTrype() {
		return type;
	}
	public void setTrype(String trype) {
		this.type = trype;
	}
	public PropertiesJson getPropertiesJson() {
		return propertiesJson;
	}
	public void setPropertiesJson(PropertiesJson propertiesJson) {
		this.propertiesJson = propertiesJson;
	}
	
}
