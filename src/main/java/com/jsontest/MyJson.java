package com.jsontest;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MyJson {

	private String type;
	@JsonProperty("features")
	private List<FeaturesJson> featuresJson;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<FeaturesJson> getFeaturesJson() {
		return featuresJson;
	}
	public void setFeaturesJson(List<FeaturesJson> featuresJson) {
		this.featuresJson = featuresJson;
	}


	
}
