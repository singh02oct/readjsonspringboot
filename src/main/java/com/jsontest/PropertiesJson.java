package com.jsontest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PropertiesJson {

	private String name;
	private String cox_site_code;
	private String cox_node_number;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCox_site_code() {
		return cox_site_code;
	}
	public void setCox_site_code(String cox_site_code) {
		this.cox_site_code = cox_site_code;
	}
	public String getCox_node_number() {
		return cox_node_number;
	}
	public void setCox_node_number(String cox_node_number) {
		this.cox_node_number = cox_node_number;
	}
	
	
	
}
